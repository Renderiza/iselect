![](http://s23.postimg.org/nckd0kugb/i_Select_Video_640_400.png)
[Video](http://youtu.be/DbuNdBGs_GI)

iSelect
=======

Filter selection by what you can see.  When you click & drag to make selection sometime unwanted entities behind get selected as well and this plugin will help you filter them out.


[More Info...](http://sketchucation.com/plugin/724-rnd_iselect_v1-0-0)

