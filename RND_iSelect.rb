module RND_Extensions
	#
	module RND_Iselect
		require 'sketchup.rb'
		require 'extensions.rb'
		#
		unless file_loaded?( __FILE__ )
			file_loaded( __FILE__ )
			# Create the extension.
			ext = SketchupExtension.new 'iSelect', 'RND_iSelect/RND_iSelect_menus.rb'
			#
			# Attach some nice info.
			ext.creator     = 'Renderiza'
			ext.version     = '1.0.2'
			ext.copyright   = '2013, Renderiza'
			ext.description = 'Filter selection by what you can see.'
			#
			# Register and load the extension on startup.
			Sketchup.register_extension ext, true
		end
		#
	end
	#
end
#
file_loaded( __FILE__ )
