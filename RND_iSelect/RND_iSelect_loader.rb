module RND_Extensions
	#
	module RND_Iselect
		#
		class Iselect
			#
			def initialize()
				#
				model = Sketchup.active_model
				sel = model.selection
				peye = model.active_view.camera.eye 
				#
				model.start_operation('iSelect')
					#
					sel.to_a.each { |e|
						boundbox = e.bounds
						#
						if e.is_a?(Sketchup::ComponentInstance) || e.is_a?(Sketchup::Group) 
							#
							c0 = boundbox.corner(0)
							c1 = boundbox.corner(1)
							c2 = boundbox.corner(2)
							c3 = boundbox.corner(3)
							c4 = boundbox.corner(4)
							c5 = boundbox.corner(5)
							c6 = boundbox.corner(6)
							c7 = boundbox.corner(7)
							#
							r0 = model.raytest [c0,peye]
							r1 = model.raytest [c1,peye]
							r2 = model.raytest [c2,peye]
							r3 = model.raytest [c3,peye]
							r4 = model.raytest [c4,peye]
							r5 = model.raytest [c5,peye]
							r6 = model.raytest [c6,peye]
							r7 = model.raytest [c7,peye]
							#
							if r0 and r1 and r4 and r5 || r2 and r3 and r6 and r7
								sel.remove e
							end
							#
						else
							#
							center = boundbox.center
							hit_path = model.raytest [center,peye]
							#
							if hit_path
								sel.remove e
							end
							#
						end
					}
					#
				model.commit_operation
				#
				Sketchup.send_action('selectSelectionTool:')
				#
				UI.beep
			end
			#
		end #class
		#
	end #module
	#
end #module
#
file_loaded( File.basename(__FILE__) ) 
