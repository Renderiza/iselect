module RND_Extensions
	#
	module RND_Iselect
		require 'sketchup.rb'
		require 'RND_iSelect/RND_iSelect_loader.rb'
	end
	#
	if !file_loaded?('rnd_menu_loader')
		@@rnd_tools_menu = UI.menu("Plugins").add_submenu("Renderiza Tools")
	end
	#
	#------New menu Items---------------------------
	if !file_loaded?('rnd_ew_loader')
		@@rnd_ew_menu = @@rnd_tools_menu.add_submenu("Websites")
		@@rnd_ew_menu.add_separator
		@@rnd_ew_menu.add_item("PluginStore"){UI.openURL('http://sketchucation.com/resources/pluginstore?pauthor=renderiza')}
		@@rnd_ew_menu.add_item("Extension Warehouse"){UI.openURL('http://extensions.sketchup.com/en/user/5451/store')}
		@@rnd_tools_menu.add_separator
		@@rnd_ew_menu.add_separator
	end
	#------------------------------------------------
	if !file_loaded?(__FILE__) then
		@@rnd_tools_menu.add_item('iSelect'){
		Sketchup.active_model.select_tool RND_Extensions::RND_Iselect::Iselect.new
		}
		# Add toolbar
		rnd_iselect_tb = UI::Toolbar.new "iSelect"
		rnd_iselect_cmd = UI::Command.new("iSelect"){
		Sketchup.active_model.select_tool RND_Extensions::RND_Iselect::Iselect.new
		}
		rnd_iselect_cmd.small_icon = "img/rnd_iselect_1_16.png"
		rnd_iselect_cmd.large_icon = "img/rnd_iselect_1_24.png"
		rnd_iselect_cmd.tooltip = "iSelect"
		rnd_iselect_cmd.status_bar_text = "Filter selection by what you can see."
		rnd_iselect_cmd.menu_text = "iSelect"
		rnd_iselect_tb = rnd_iselect_tb.add_item rnd_iselect_cmd
		rnd_iselect_tb.show
	end
	#
	#------Add Context Menu-------------------------
	UI.add_context_menu_handler{|menu|
		#
		menu.add_item('iSelect'){
		Sketchup.active_model.select_tool RND_Extensions::RND_Iselect::Iselect.new
		}
	}
	#
	file_loaded('rnd_ew_loader')
	file_loaded('rnd_menu_loader')
	file_loaded(__FILE__)
end
